package com.example.playground

import android.graphics.pdf.PdfDocument
import androidx.appcompat.app.AppCompatActivity
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfRenderer
import android.net.Uri
import android.os.*
import androidx.annotation.RequiresApi
import android.graphics.Bitmap
import android.os.Build
import kotlinx.android.synthetic.main.activity_pdf_viewer.*
import android.print.PrintAttributes
import android.print.pdf.PrintedPdfDocument
import android.view.View
import androidx.core.content.FileProvider


class PdfViewer : AppCompatActivity() {

    private lateinit var pdfRenderer: PdfRenderer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
        setContentView(R.layout.activity_pdf_viewer)
        val uri = FileProvider.getUriForFile(
            this,
            this.applicationContext.packageName + ".provider",
            File(this.getExternalFilesDir(null)?.path + "/mypdfs/test.pdf")
        )
        openWithButton.setOnClickListener {
            createFile(uri)
        }
        shareButton.setOnClickListener {
            sharePDF(uri)
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        createPDF()
    }

    private fun createPDF() {
        val printattr = PrintAttributes.Builder()
            .setColorMode(PrintAttributes.COLOR_MODE_COLOR)
            .setMediaSize(PrintAttributes.MediaSize.ISO_A4)
            .setMinMargins(PrintAttributes.Margins.NO_MARGINS)
            .build()
        val pdfDocument = PrintedPdfDocument(this, printattr)
        val pageInfo = PdfDocument.PageInfo.Builder(595, 842, 1).create()

        val page = pdfDocument.startPage(pageInfo)

        val paint = Paint()
        paint.color = Color.BLACK

        val view = layoutInflater.inflate(R.layout.activity_pdf_viewer, null)
        val measureWidth =
            View.MeasureSpec.makeMeasureSpec(page.canvas.width, View.MeasureSpec.EXACTLY)
        val measuredHeight =
            View.MeasureSpec.makeMeasureSpec(page.canvas.height, View.MeasureSpec.EXACTLY)

        view.measure(measureWidth, measuredHeight)
        view.layout(0, 0, page.canvas.width, page.canvas.height)
        view.draw(page.canvas)

        pdfDocument.finishPage(page)

        val directoryPath = this.getExternalFilesDir(null)?.path + "/mypdfs/"
        val file = File(directoryPath)
        if (!file.exists()) {
            file.mkdirs()
        }
        val targetPDF = directoryPath + "test.pdf"
        val filePath = File(targetPDF)

        pdfDocument.writeTo(FileOutputStream(filePath))

        pdfDocument.close()

        val uri = Uri.fromFile(filePath)
        openRenderer(targetPDF)
    }

    private fun createFile(pickerInitialUri: Uri) {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            setDataAndType(pickerInitialUri, "application/pdf")
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            // Optionally, specify a URI for the directory that should be opened in
            // the system file picker before your app creates the document.
        }
        startActivityForResult(intent, 1)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Throws(IOException::class)
    private fun openRenderer(
        targetPDF: String
    ) {
        val parcelFileDescriptor =
            ParcelFileDescriptor.open(File(targetPDF), ParcelFileDescriptor.MODE_READ_ONLY)
        // This is the PdfRenderer we use to render the PDF.
        if (parcelFileDescriptor != null) {
            pdfRenderer = PdfRenderer(parcelFileDescriptor)
        }
        showPage(0)
    }

    private fun sharePDF(pickerInitialUri: Uri) {
        val shareIntent = Intent(Intent(Intent.ACTION_SEND)).apply {
            setDataAndType(pickerInitialUri, "application/pdf")
            putExtra(Intent.EXTRA_SUBJECT, "Sharing file..")
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }
        startActivity(Intent.createChooser(shareIntent, "Share file"))
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private fun showPage(index: Int) {
        // Make sure to close the current page before opening another one.

        // Use `openPage` to open a specific page in PDF.
        val currentPage = pdfRenderer.openPage(0)
        // Important: the destination bitmap must be ARGB (not RGB).
        val bitmap = Bitmap.createBitmap(
            currentPage.getWidth(), currentPage.getHeight(),
            Bitmap.Config.ARGB_8888
        )
        // Here, we render the page onto the Bitmap.
        // To render a portion of the page, use the second and third parameter. Pass nulls to get
        // the default result.
        // Pass either RENDER_MODE_FOR_DISPLAY or RENDER_MODE_FOR_PRINT for the last parameter.
        currentPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)
        // We are ready to show the Bitmap to user.
        //pdf_image.setImageBitmap(bitmap)
    }
}
